/**
 * Represents the auth flag configuration found on the `chromia.yml` file.
 * It can be modified under `moduleArgs -> lib.ft4.core.accounts -> auth_flags`
 */
struct auth_flags_config {
    /** All auth descriptors must have these flags to exist */
    mandatory: gtv = [auth_flags.ACCOUNT, auth_flags.TRANSFER].to_gtv();
    /**
     * All auth descriptors will have these flags on creation.
     * If null, it will be set to the value in `mandatory`.
     */
    default: gtv? = null;
}

// UNUSED
/** retrieve the settings for auth flags written in the `chromia.yml` file, unparsed */
function get_raw_auth_flags_config() {
    return chain_context.args.auth_flags;
}

/** retrieve the settings for auth flags written in the `chromia.yml` file */
function get_auth_flags_config() {
    val flags = chain_context.args.auth_flags;
    val mandatory = parse_auth_flags(flags.mandatory);
    val default = if (flags.default??) parse_auth_flags(flags.default!!) else mandatory;

    return (
        mandatory = mandatory,
        default = default
    );
}

/**
 * Parse the values in the `gtv` as auth flags. Used to retrieve the configuration found in
 * the `chromia.yml` file for the auth flag config.
 * 
 * Throws `"INVALID FLAGS"` if the value is:
 * - a list of anything else than just text values
 * - neither a list nor a comma separated text
 * 
 * Throws if the flags are not valid.
 * 
 * @see require_valid_auth_flags for information on what flags are valid
 */
function parse_auth_flags(gtv) {
    var flags = list<text>();
    
    if (utils.is_list(gtv)) {
        flags = require(
            try_call(list<text>.from_gtv(gtv, *)),
            "INVALID FLAGS: Cannot parse auth flags. Expected flags list, but found <%s>".format(gtv)
        );
    } else if (utils.is_text(gtv)) {
        flags = (text.from_gtv(gtv).split(",")) @* {} ($.trim());
    } else {
        require(false, "INVALID FLAGS: Expected flags list or comma separated flags list, but found <%s>".format(gtv));
    }

    require_valid_auth_flags(flags);

    return flags;
}

/**
 * Checks that all flags values passed are composed of letters and/or underscores.
 * 
 * Throws `"INVALID FLAGS"` if any flag does not match `/[a-z_A-Z]+/`
 */
function require_valid_auth_flags(flags: list<text>) {
    val invalid_flags = flags @* { not $.matches("[a-z_A-Z]+") };
    require(
        empty(invalid_flags),
        "INVALID FLAGS: Found invalid flags <%s>".format(invalid_flags)
    );
}
