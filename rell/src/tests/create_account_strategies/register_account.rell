@test
module;

import lib.ft4.accounts;
import lib.ft4.accounts.strategies;
import lib.ft4.accounts.strategies.open;
import test: lib.ft4.test.core;
import test_op: lib.ft4.test.operations;


function test_is_strategy_op_returns_true_when_receives_strategy_operation() {
    val auth_descriptor = test.create_auth_descriptor(rell.test.pubkeys.alice);
    assert_true(strategies.is_strategy_op(open.ras_open(auth_descriptor).to_gtx_operation()));
}

function test_is_strategy_op_returns_false_when_receives_non_strategy_operation() {
    assert_false(strategies.is_strategy_op(test_op.empty_op().to_gtx_operation()));
}

function test_registering_account_adds_main_auth_descriptor() {
    val main_auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.pubkeys.alice,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(main_auth_descriptor, null))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .run();

    val account = accounts.account @? { .id == rell.test.pubkeys.alice.hash() };
    assert_not_null(account);

    assert_equals(
        accounts.main_auth_descriptor @ { account } (.auth_descriptor.id),
        main_auth_descriptor.hash()
    );

    assert_equals(
        accounts.account_auth_descriptor @* { account } (.id),
        [ main_auth_descriptor.hash() ]
    );
}

function test_registering_account_adds_disposable_auth_descriptor() {
    val main_auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.pubkeys.alice,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    val disposable_auth_descriptor = accounts.single_sig_auth_descriptor(rell.test.pubkeys.bob, set([accounts.auth_flags.TRANSFER]));

    rell.test.tx()
        .op(open.ras_open(main_auth_descriptor, disposable_auth_descriptor))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    val account = accounts.account @? { .id == rell.test.pubkeys.alice.hash() };
    assert_not_null(account);

    assert_equals(
        accounts.main_auth_descriptor @ { account } (.auth_descriptor.id),
        main_auth_descriptor.hash()
    );

    assert_equals(
        accounts.account_auth_descriptor @* { account } (@sort .id),
        [ main_auth_descriptor.hash(), disposable_auth_descriptor.hash() ].sorted()
    );
}

function test_account_registration_fails_when_mandatory_flags_missing_in_main_auth_descriptor() {
    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .run_must_fail("Some of mandatory flags <[A, T]> missing, found only <[A]>");    
}

function test_get_enabled_registration_strategies() {
    assert_equals(
        strategies.external.get_enabled_registration_strategies(),
        set(["ft4.ras_open", "ft4.ras_transfer_open", "ft4.ras_transfer_fee", "ft4.ras_transfer_subscription"])
    );
}
