@test
module;

import lib.ft4.assets;
import lib.ft4.admin.crosschain;
import test: lib.ft4.test.core;

function test_can_fetch_crosschain_asset_registration_details() {
    val issuing_blockchain_rid = x"aa".repeat(32);
    val origin_blockchain_rid = x"bb".repeat(32);
    val asset_id = ("Test asset", issuing_blockchain_rid).hash();
    crosschain.external.register_crosschain_asset(
        id = asset_id,
        name = "Test asset",
        symbol = "TST",
        decimals = 6,
        issuing_blockchain_rid = issuing_blockchain_rid,
        icon_url = "https://filehub.chromia.com/project/collection/1",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"aabbccdd",
        origin_blockchain_rid = origin_blockchain_rid
    )
        .sign(test.admin_priv_key())
        .run();

    val asset = assets.external.get_asset_details_for_crosschain_registration(asset_id);
    
    assert_equals(
        asset,
        (
            id = asset_id,
            name = "Test asset",
            symbol = "TST",
            decimals = 6,
            blockchain_rid = issuing_blockchain_rid,
            icon_url = "https://filehub.chromia.com/project/collection/1",
            type = assets.ASSET_TYPE_FT4,
            uniqueness_resolver = x"aabbccdd"
        )
    );
}

function test_crosschain_asset_registration_fails_when_name_is_longer_than_1024_characters() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "x".repeat(1025),
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Asset name cannot be longer than 1024 characters");    
}

function test_crosschain_asset_registration_fails_when_symbol_is_longer_than_1024_characters() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "s".repeat(1025),
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Asset symbol cannot be longer than 1024 characters");    
}

function test_crosschain_asset_registration_fails_when_asset_id_has_less_than_32_bytes() {
    crosschain.external.register_crosschain_asset(
        id = x"aaaaaa",
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Invalid asset ID length <3>, must be <32>");       
}

function test_crosschain_asset_registration_fails_when_asset_id_has_more_than_32_bytes() {
    crosschain.external.register_crosschain_asset(
        id = x"aa".repeat(40),
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Invalid asset ID length <40>, must be <32>");       
}

function test_crosschain_asset_registration_fails_when_asset_type_is_empty_string() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = "",
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Asset type cannot be an empty string");    
}

function test_crosschain_asset_registration_fails_when_asset_type_is_longer_than_1024_characters() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = "t".repeat(1025),
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Asset type cannot be longer than 1024 characters");    
}

function test_crosschain_asset_registration_fails_when_asset_uniqueness_resolver_is_longer_than_1024_bytes() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"bb".repeat(1025),
        origin_blockchain_rid = x"92F32F448B7D3C979854765E69BC574E8372618A2E857C797C4CAD4D172B6967"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Asset uniqueness resolver cannot be longer than 1024 characters");    
}

function test_crosschain_asset_registration_fails_when_asset_issuing_blockchain_rid_is_longer_than_32_bytes() {
    val issuing_blockchain_rid = x"aa".repeat(33);
    val origin_blockchain_rid = x"bb".hash();
    val asset_id = ("Test asset", issuing_blockchain_rid).hash();
    crosschain.external.register_crosschain_asset(
        id = asset_id,
        name = "Test asset",
        symbol = "TST",
        decimals = 6,
        issuing_blockchain_rid = issuing_blockchain_rid,
        icon_url = "https://filehub.chromia.com/project/collection/1",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"aabbccdd",
        origin_blockchain_rid = origin_blockchain_rid
    )
    .sign(test.admin_priv_key())
    .run_must_fail("issuing_blockchain_rid <%s> has invalid length. Expected <32> bytes, found <33>".format(issuing_blockchain_rid));
}

function test_crosschain_asset_registration_fails_when_asset_origin_blockchain_rid_is_longer_than_32_bytes() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    val origin_blockchain_rid = x"cc".repeat(33);
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = origin_blockchain_rid
    )
    .sign(test.admin_priv_key())
    .run_must_fail("origin_blockchain_rid <%s> has invalid length. Expected <32> bytes, found <33>".format(origin_blockchain_rid));    
}

function test_crosschain_asset_registration_fails_when_asset_origin_blockchain_rid_is_current_rid() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F",
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = chain_context.blockchain_rid
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Asset origin cannot be the current chain.");    
}

function test_crosschain_asset_registration_fails_when_asset_issuer_is_this_blockchain() {
    val id = ("name", x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F").hash();
    crosschain.external.register_crosschain_asset(
        id = id,
        name = "name",
        symbol = "SYM",
        decimals = 2,
        issuing_blockchain_rid = chain_context.blockchain_rid,
        icon_url = "",
        type = assets.ASSET_TYPE_FT4,
        uniqueness_resolver = x"",
        origin_blockchain_rid = x"BBE58F50E00C733113F8E0647D53FD4BFAD1D85AB831B1719BAF20F1A3336C8F"
    )
    .sign(test.admin_priv_key())
    .run_must_fail("Cannot register an asset that originated on this chain as a crosschain asset.");    
}
