@test module;

import lib.ft4.auth;
import lib.ft4.assets;
import xc: lib.ft4.crosschain;
import ^^.extensions;
import admin_crosschain: ^^^.lib.ft4.admin.crosschain;
import test_op: lib.ft4.test.operations;
import test: lib.ft4.test.core;
import ^^.operations.iccf_op.{ iccf_proof };

function test_can_revert_canceled_transfer_once_but_not_twice() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val target_blockchain_rid = x"DDDD";
    test.mint(alice.account, asset_valid, 10L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [target_blockchain_rid],
            deadline = rell.test.last_block_time+1
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    assert_equals(assets.get_asset_balance(alice.account, asset_valid), 5L);

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(target_blockchain_rid, cancel_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, cancel_tx, 0))
        .nop()
        .run();

    assert_equals(assets.get_asset_balance(alice.account, asset_valid), 10L);

    val revert_tx_rid = test.get_last_transaction().tx_rid;

    val history = assets.external.get_transfer_history(alice.account.id, assets.filter(null), null, null)!!.data;
    val history_entry = map<text,gtv>.from_gtv(history[0]);
    assert_equals(history_entry["tx_rid"], revert_tx_rid.to_gtv());
    assert_equals(history_entry["op_index"], (1).to_gtv());
    assert_equals(history_entry["operation_name"], rell.meta(xc.external.revert_transfer).mount_name.to_gtv());
    assert_equals(history_entry["delta"], 5L.to_gtv());
    assert_equals(history_entry["is_input"], (0).to_gtv());
    assert_equals(history_entry["is_crosschain"], (1).to_gtv());

    val expected = [
        assets.transfer_detail(
            blockchain_rid = chain_context.blockchain_rid,
            account_id = alice.account.id,
            asset_id = asset_valid.id,
            delta = 5L,
            is_input = false
        ),
        assets.transfer_detail(
            blockchain_rid = target_blockchain_rid,
            account_id = recipient_account_id,
            asset_id = asset_valid.id,
            delta = 5L,
            is_input = true
        )
    ];
    assert_equals(assets.external.get_transfer_details(revert_tx_rid, 1), expected);
    assert_equals(assets.external.get_transfer_details_by_asset(revert_tx_rid, 1, asset_valid.id), expected);

    rell.test.tx()
        .op(iccf_proof(target_blockchain_rid, cancel_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, cancel_tx, 0))
        .nop()
        .run_must_fail("Transaction <%s> transfer at index <%s> has already been reverted on this chain."
                  .format(init_tx.body.hash(), 1));
}

function test_can_revert_unapplied_transfer_once_but_not_twice() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val intermediate_blockchain_rid = x"CCCC";
    val target_blockchain_rid = x"DDDD";
    test.mint(alice.account, asset_valid, 10L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [intermediate_blockchain_rid, target_blockchain_rid],
            deadline = rell.test.last_block_time+1
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    assert_equals(assets.get_asset_balance(alice.account, asset_valid), 5L);

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 1
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    val unapply_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = intermediate_blockchain_rid,
            operations = [
                xc.external.unapply_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    last_tx = cancel_tx,
                    last_op_index = 0,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(intermediate_blockchain_rid, unapply_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, unapply_tx, 0))
        .nop()
        .run();

    assert_equals(assets.get_asset_balance(alice.account, asset_valid), 10L);

    val revert_tx_rid = test.get_last_transaction().tx_rid;

    val history = assets.external.get_transfer_history(alice.account.id, assets.filter(null), null, null)!!.data;
    val history_entry = map<text,gtv>.from_gtv(history[0]);
    assert_equals(history_entry["tx_rid"], revert_tx_rid.to_gtv());
    assert_equals(history_entry["op_index"], (1).to_gtv());
    assert_equals(history_entry["operation_name"], rell.meta(xc.external.revert_transfer).mount_name.to_gtv());
    assert_equals(history_entry["delta"], 5L.to_gtv());
    assert_equals(history_entry["is_input"], (0).to_gtv());
    assert_equals(history_entry["is_crosschain"], (1).to_gtv());

    val expected = [
        assets.transfer_detail(
            blockchain_rid = chain_context.blockchain_rid,
            account_id = alice.account.id,
            asset_id = asset_valid.id,
            delta = 5L,
            is_input = false
        ),
        assets.transfer_detail(
            blockchain_rid = target_blockchain_rid,
            account_id = recipient_account_id,
            asset_id = asset_valid.id,
            delta = 5L,
            is_input = true
        )
    ];
    assert_equals(assets.external.get_transfer_details(revert_tx_rid, 1), expected);
    assert_equals(assets.external.get_transfer_details_by_asset(revert_tx_rid, 1, asset_valid.id), expected);

    rell.test.tx()
        .op(iccf_proof(intermediate_blockchain_rid, unapply_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, unapply_tx, 0))
        .nop()
        .run_must_fail("Transaction <%s> transfer at index <%s> has already been reverted on this chain."
                  .format(init_tx.body.hash(), 1));
}

function test_can_not_revert_canceled_transfer_before_expiry() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val target_blockchain_rid = x"DDDD";
    test.mint(alice.account, asset_valid, 10L);

    val deadline = rell.test.last_block_time + rell.test.DEFAULT_BLOCK_INTERVAL * 2;

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [target_blockchain_rid],
            deadline = deadline
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(target_blockchain_rid, cancel_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, cancel_tx, 0))
        .nop()
        .run_must_fail("This transfer has not yet expired. It can not be reverted before <%s> (evaluated at timestamp <%s>).".format(deadline, rell.test.last_block_time));
}

function test_can_not_revert_non_existing_transfer() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val target_blockchain_rid = x"DDDD";
    test.mint(alice.account, asset_valid, 10L);

    val init_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = chain_context.blockchain_rid,
            operations = [
                auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id).to_gtx_operation(),
                xc.external.init_transfer(
                    recipient_id = recipient_account_id,
                    asset_id = asset_valid.id,
                    amount = 5L,
                    hops = [target_blockchain_rid],
                    deadline = rell.test.last_block_time-1
                ).to_gtx_operation()
            ],
            signers = [rell.test.pubkeys.alice.to_gtv()]
        ),
        signatures = [x"".to_gtv()]
    );

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(target_blockchain_rid, cancel_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, cancel_tx, 0))
        .nop()
        .run_must_fail("Cannot revert cross-chain transfer for transaction <%s> at index <%d>. init_transfer not found."
                  .format(init_tx.body.hash(), 1));
}

function test_can_not_revert_without_proof() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val target_blockchain_rid = x"DDDD";
    test.mint(alice.account, asset_valid, 10L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [target_blockchain_rid],
            deadline = rell.test.last_block_time+1
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(xc.external.revert_transfer(init_tx, 1, cancel_tx, 0))
        .nop()
        .run_must_fail("No proof operation present for TX");
}

function test_can_not_revert_with_wrong_op() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val target_blockchain_rid = x"DDDD";
    test.mint(alice.account, asset_valid, 10L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [target_blockchain_rid],
            deadline = rell.test.last_block_time+1
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val bogus_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                test_op.empty_op().to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(target_blockchain_rid, bogus_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, bogus_tx, 0))
        .nop()
        .run_must_fail("last_tx[last_op_index] must be ft4.crosschain.cancel_transfer, ft4.crosschain.unapply_transfer or ft4.crosschain.recall_unclaimed_transfer");
}

function test_can_not_revert_transfer_canceled_on_wrong_chain() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val target_blockchain_rid = x"DDDD";
    val wrong_blockchain_rid = x"FFFF";
    test.mint(alice.account, asset_valid, 10L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [target_blockchain_rid],
            deadline = rell.test.last_block_time+1
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = wrong_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(wrong_blockchain_rid, cancel_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, cancel_tx, 0))
        .nop()
        .run_must_fail(
            "Argument mismatch: next hops (%s) and tx blockchain_rid (%s) don't correspond"
            .format(target_blockchain_rid, wrong_blockchain_rid)
        );
}

function test_can_not_revert_transfer_unapplied_on_wrong_chain() {
    val alice = test.register_alice();
    val recipient_account_id = "the recipient".hash();
    val recipient_auth_descriptor_id = "recipient auth descriptor".hash();
    val asset_valid = test.create_asset("validAsset", "va", 1);

    val intermediate_blockchain_rid = x"CCCC";
    val target_blockchain_rid = x"DDDD";
    val wrong_blockchain_rid = x"FFFF";
    test.mint(alice.account, asset_valid, 10L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(xc.external.init_transfer(
            recipient_account_id,
            asset_valid.id,
            5L,
            [intermediate_blockchain_rid, target_blockchain_rid],
            deadline = rell.test.last_block_time+1
        ))
        .sign(rell.test.keypairs.alice)
        .run();

    val init_tx = gtx_transaction.from_bytes(test.get_last_transaction().tx_data);

    val cancel_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = target_blockchain_rid,
            operations = [
                xc.external.cancel_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    previous_hop_tx = init_tx,
                    op_index = 1,
                    hop_index = 1
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    val unapply_tx = gtx_transaction(
        body = gtx_transaction_body(
            blockchain_rid = wrong_blockchain_rid,
            operations = [
                xc.external.unapply_transfer(
                    init_transfer_tx = init_tx,
                    init_tx_op_index = 1,
                    last_tx = cancel_tx,
                    last_op_index = 0,
                    hop_index = 0
                ).to_gtx_operation()
            ],
            signers = []
        ),
        signatures = []
    );

    rell.test.tx()
        .op(iccf_proof(wrong_blockchain_rid, unapply_tx.to_gtv().hash(), x"", x"", 0, x""))
        .op(xc.external.revert_transfer(init_tx, 1, unapply_tx, 0))
        .nop()
        .run_must_fail(
            "Argument mismatch: next hops (%s) and tx blockchain_rid (%s) don't correspond"
            .format(intermediate_blockchain_rid, wrong_blockchain_rid)
        );
}
