@test module;

import lib.ft4.auth;
import lib.ft4.accounts;
import test_op: lib.ft4.test.operations;
import test: lib.ft4.test.core;

function test_returns_ad_to_be_deleted_when_in_list() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);

    test_op.add_ad_to_account(alice.account, new_ad)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    val args = struct<accounts.external.delete_auth_descriptor>(
        auth_descriptor_id=new_ad.hash()
    ).to_gtv();
    val selected_id = accounts.external.delete_auth_descriptor_resolver(args, alice.account.id, [new_ad.hash(), alice.auth_descriptor_id]);
    assert_equals(selected_id, new_ad.hash());
}

function test_returns_ad_with_account_flag_if_deleting_other_ad() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);
    val new_ad2 = test.create_auth_descriptor(rell.test.keypairs.charlie.pub);

    rell.test.tx()
        .op(test_op.add_ad_to_account(alice.account, new_ad))
        .op(test_op.add_ad_to_account(alice.account, new_ad2))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .sign(rell.test.keypairs.charlie)
        .run();

    val args = struct<accounts.external.delete_auth_descriptor>(
        auth_descriptor_id=new_ad2.hash()
    ).to_gtv();
    val selected_id = accounts.external.delete_auth_descriptor_resolver(
        args, 
        alice.account.id, 
        [new_ad.hash(), alice.auth_descriptor_id]
    );
    assert_equals(selected_id, alice.auth_descriptor_id);
}

function test_returns_null_if_no_valid_ads_in_list() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);

    test_op.add_ad_to_account(alice.account, new_ad)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    val args = struct<accounts.external.delete_auth_descriptor>(
        auth_descriptor_id=alice.auth_descriptor_id
    ).to_gtv();
    
    val selected_id = accounts.external.delete_auth_descriptor_resolver(
        args, 
        alice.account.id, 
        [new_ad.hash()]
    );
    assert_null(selected_id);
}

function test_returns_null_if_provided_auth_descriptor_is_not_main_auth_descriptor() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);

    test_op.add_ad_to_account(alice.account, new_ad)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    val args = struct<accounts.external.delete_all_auth_descriptors_except_main>().to_gtv();
    
    val selected_id = accounts.external.delete_all_auth_descriptors_except_main_resolver(
        args, 
        alice.account.id, 
        [new_ad.hash()]
    );
    assert_null(selected_id);
}

function test_can_delete_ad_if_signed_with_ad_with_account_flag() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);

    test_op.add_ad_to_account(alice.account, new_ad)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.alice.pub))
        .op(accounts.external.delete_auth_descriptor(new_ad.hash()))
        .sign(rell.test.keypairs.alice)
        .run();
    
    assert_equals(
        accounts.account_auth_descriptor @ { .account == alice.account }( .id ),
        alice.auth_descriptor_id
    );
}

function test_can_not_delete_ad_if_signed_with_ad_without_account_flag() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);
    val new_ad2 = test.create_auth_descriptor(rell.test.keypairs.charlie.pub);

    rell.test.tx()
        .op(test_op.add_ad_to_account(alice.account, new_ad))
        .op(test_op.add_ad_to_account(alice.account, new_ad2))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .sign(rell.test.keypairs.charlie)
        .run();
    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.charlie.pub))
        .op(accounts.external.delete_auth_descriptor(new_ad.hash()))
        .sign(rell.test.keypairs.charlie)
        .run_must_fail("The provided auth descriptor is not valid for this operation");
}

function test_can_delete_ads_for_signer_if_signed_with_ad_with_account_flag() {
    val alice = test.register_alice();
    val new_ad1 = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.TRANSFER, "X"]);
    val new_ad2 = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.TRANSFER, "Y"]);

    test_op.add_ad_to_account(alice.account, new_ad1)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    test_op.add_ad_to_account(alice.account, new_ad2)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.bob.pub).size(),
        2
    );

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.alice.pub))
        .op(accounts.external.delete_auth_descriptors_for_signer(rell.test.keypairs.bob.pub))
        .sign(rell.test.keypairs.alice)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.bob.pub).size(),
        0
    );
}

function test_can_delete_multisig_ads_for_signer_if_signed_with_ad_with_account_flag() {
    val alice = test.register_alice();
    val new_ad1 = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.TRANSFER, "X"]);
    val new_ad2 = test.create_multisig_auth_descriptor(1, [rell.test.keypairs.charlie.pub, rell.test.keypairs.dave.pub]);

    test_op.add_ad_to_account(alice.account, new_ad1)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    test_op.add_ad_to_account(alice.account, new_ad2)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.charlie)
        .sign(rell.test.keypairs.dave)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.charlie.pub).size(),
        1
    );
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.dave.pub).size(),
        1
    );

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.alice.pub))
        .op(accounts.external.delete_auth_descriptors_for_signer(rell.test.keypairs.charlie.pub))
        .sign(rell.test.keypairs.alice)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.charlie.pub).size(),
        0
    );
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.dave.pub).size(),
        0
    );
}

function test_can_not_delete_ads_for_signer_if_signed_with_ad_without_account_flag() {
    val alice = test.register_alice();
    val new_ad1 = test.create_auth_descriptor(rell.test.keypairs.bob.pub);
    val new_ad2 = test.create_auth_descriptor(rell.test.keypairs.charlie.pub);

    test_op.add_ad_to_account(alice.account, new_ad1)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    test_op.add_ad_to_account(alice.account, new_ad2)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.charlie)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.bob.pub).size(),
        1
    );

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.charlie.pub))
        .op(accounts.external.delete_auth_descriptors_for_signer(rell.test.keypairs.bob.pub))
        .sign(rell.test.keypairs.charlie)
        .run_must_fail("The provided auth descriptor is not valid for this operation");
}

function test_ad_can_delete_itself_for_signer_without_account_flag() {
    val alice = test.register_alice();
    val new_ad1 = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.TRANSFER, "X"]);
    val new_ad2 = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.TRANSFER, "Y"]);

    test_op.add_ad_to_account(alice.account, new_ad1)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    test_op.add_ad_to_account(alice.account, new_ad2)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.bob.pub).size(),
        2
    );

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.bob.pub))
        .op(accounts.external.delete_auth_descriptors_for_signer(rell.test.keypairs.bob.pub))
        .sign(rell.test.keypairs.bob)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.bob.pub).size(),
        0
    );
}

function test_can_not_delete_multisig_ads_for_signer_if_signed_with_ad_without_account_flag() {
    val alice = test.register_alice();
    val new_ad1 = test.create_auth_descriptor(rell.test.keypairs.bob.pub);
    val new_ad2 = test.create_multisig_auth_descriptor(1, [rell.test.keypairs.charlie.pub, rell.test.keypairs.dave.pub]);

    test_op.add_ad_to_account(alice.account, new_ad1)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
    test_op.add_ad_to_account(alice.account, new_ad2)
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.charlie)
        .sign(rell.test.keypairs.dave)
        .run();
    assert_equals(
        accounts.get_auth_descriptors_by_signer(alice.account.id, rell.test.keypairs.bob.pub).size(),
        1
    );

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.charlie.pub))
        .op(accounts.external.delete_auth_descriptors_for_signer(rell.test.keypairs.charlie.pub))
        .sign(rell.test.keypairs.charlie)
        .sign(rell.test.keypairs.dave)
        .run_must_fail("The provided auth descriptor is not valid for this operation");
}

function test_can_delete_all_ad_except_main_when_authorized_with_main_auth_descriptor() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);
    val new_ad2 = test.create_auth_descriptor(rell.test.keypairs.charlie.pub, [accounts.auth_flags.ACCOUNT]);

    rell.test.tx()
        .op(test_op.add_ad_to_account(alice.account, new_ad))
        .op(test_op.add_ad_to_account(alice.account, new_ad2))
        .sign(alice.keypair)
        .sign(rell.test.keypairs.bob)
        .sign(rell.test.keypairs.charlie)
        .run();

    rell.test.tx()
        .op(test.ft_auth_operation_for(alice.keypair.pub))
        .op(accounts.external.delete_all_auth_descriptors_except_main())
        .sign(alice.keypair)
        .run();

    assert_equals(
        accounts.account_auth_descriptor @* { .account == alice.account }( .id ),
        [alice.auth_descriptor_id]
    );
}

function test_can_not_delete_all_ad_except_main_when_called_with_auth_descriptor_that_is_not_main() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub);

    rell.test.tx()
        .op(test_op.add_ad_to_account(alice.account, new_ad))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.bob.pub))
        .op(accounts.external.delete_all_auth_descriptors_except_main())
        .sign(rell.test.keypairs.bob)
        .run_must_fail("The provided auth descriptor is not valid for this operation");
}

function test_cannot_delete_all_ad_except_main_when_called_with_auth_descriptor_with_A_flag_that_is_not_main() {
    val alice = test.register_alice();
    val new_ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.ACCOUNT]);

    rell.test.tx()
        .op(alice.ft_auth(accounts.external.add_auth_descriptor(new_ad)))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.bob.pub))
        .op(accounts.external.delete_all_auth_descriptors_except_main())
        .sign(rell.test.keypairs.bob)
        .run_must_fail("The provided auth descriptor is not valid for this operation");
}

function test_ad_can_delete_itself_without_account_flag() {
  val account_data = test.register_account_open(rell.test.keypairs.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  val ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.TRANSFER]);
  rell.test
    .tx(test_op.add_ad_to_account(account_data.account, ad))
    .sign(rell.test.keypairs.bob)
    .sign(rell.test.keypairs.alice)
    .run();

  val ad_id = accounts.account_auth_descriptor @ {} ( @sort_desc @omit .rowid, .id ) limit 1;
  val ads = accounts.account_auth_descriptor @* { .account.id == account_data.account.id };
  assert_equals(ads.size(), 2);
  assert_equals(ads[1].id, ad_id);
  
  rell.test.tx()
    .op(test.ft_auth_operation_for(rell.test.keypairs.bob.pub))
    .op(accounts.external.delete_auth_descriptor(ad_id))
    .sign(rell.test.keypairs.bob)
    .run();
  
  val remaining_ads = accounts.account_auth_descriptor @* { .account.id == account_data.account.id };
  assert_equals(remaining_ads.size(), 1);
  assert_equals(remaining_ads[0].id, account_data.auth_descriptor_id);
}

function test_op_delete_auth_descriptor_doesnt_fail_if_ad_expired() {
  val account_data = test.register_account_open(rell.test.keypairs.alice, ["A", "T"]);
  val latest_height = block @ {} (@sort_desc .block_height) limit 1;
  val ad = test.create_auth_descriptor(
    rell.test.keypairs.alice.pub, 
    ["A"], 
    accounts.serialize_rules([accounts.rule_expression
        (accounts.rule_operator.lt, accounts.rule_variable.block_height, latest_height + 2)
    ])
  );
  rell.test
    .tx(test_op.add_ad_to_account(account_data.account, ad))
    .sign(rell.test.keypairs.alice)
    .run();
  val ads = accounts.account_auth_descriptor @* { .account.id == account_data.account.id };

  assert_equals(ads.size(), 2);  
  rell.test.block().run();
  rell.test.block().run();
  val ad_id = accounts.account_auth_descriptor @ {} ( @sort_desc @omit .rowid, .id ) limit 1;
  rell.test.tx()
    .op(test.ft_auth_operation_for(rell.test.keypairs.alice.pub))
    .op(accounts.external.delete_auth_descriptor(ad_id))
    .sign(rell.test.keypairs.alice)
    .run();
  val ads_after = accounts.account_auth_descriptor @* { .account.id == account_data.account.id };   
  assert_equals(ads_after.size(), 1);  
}

function test_the_remaining_ad_is_selected_when_calling_delete_all_auth_descriptors_exclude() {
  val account_data = test.register_account_open(rell.test.keypairs.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  val ad = test.create_auth_descriptor(rell.test.keypairs.bob.pub, [accounts.auth_flags.ACCOUNT]);
  rell.test
    .tx(test_op.add_ad_to_account(account_data.account, ad))
    .sign(rell.test.keypairs.bob)
    .sign(rell.test.keypairs.alice)
    .run();

  val ad_id = accounts.account_auth_descriptor @ {} ( @sort_desc @omit .rowid, .id ) limit 1;
  val ads = accounts.account_auth_descriptor @* { .account.id == account_data.account.id };

  assert_equals(ads.size(), 2);
  assert_equals(ads[1].id, ad_id);

  val op_args = struct<accounts.external.delete_all_auth_descriptors_except_main>().to_gtv();

  val selected_ad_id = auth.external.get_first_allowed_auth_descriptor(
    "ft4.delete_all_auth_descriptors_except_main",
    op_args,
    account_data.account.id,
    accounts.account_auth_descriptor @* {} ( .id )
  );

  assert_equals(selected_ad_id, account_data.auth_descriptor_id);
}
