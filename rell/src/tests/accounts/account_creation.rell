@test module;

import test_op: ^^.operations;
import ^.helpers;
import lib.ft4.accounts;
import lib.ft4.admin;
import test: lib.ft4.test.core;
import auth: ^^.operations.auth;
import lib.ft4.accounts.strategies;
import lib.ft4.accounts.strategies.open;

val alice = rell.test.keypairs.alice;
val bob = rell.test.keypairs.bob;

function test_an_account_can_be_created() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .run();
  
  assert_not_null(accounts.account @? { alice.pub.hash() });
}

function test_main_ad_is_registered() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .run();

  val ad_id = auth_descriptor.hash();
  val acc = accounts.account @? { alice.pub.hash() };
  
  assert_not_null(acc);
  assert_equals(accounts.main_auth_descriptor @ { acc } (.auth_descriptor.id), ad_id);
}

function test_cannot_create_account_with_no_A_flag() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.TRANSFER]);
  
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .run_must_fail("Some of mandatory flags <[A, T]> missing, found only <[T]>");
}

function test_cannot_create_account_with_expiring_ad() {
  val auth_descriptor = test.create_auth_descriptor(
    rell.test.pubkeys.bob,
    [accounts.auth_flags.ACCOUNT],
    accounts.serialize_rules([
      accounts.less_than(accounts.block_height(100))
    ])
  );
  
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .sign(bob)
    .run_must_fail("Cannot create an account with a restricted auth descriptor");
}

function test_multi_sig_auth_descriptor_can_be_added() {
  val auth_descriptor = accounts.auth_descriptor(
    accounts.auth_type.M,
    [
        [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER].to_gtv(),
        (2).to_gtv(),
        [
          alice.pub,
          bob.pub
        ].to_gtv()
    ],
    rules = null.to_gtv()
  );

  test.register_account_with_multisig_auth_descriptor(auth_descriptor, [alice, bob]);
  
  assert_not_null(accounts.account @? { [alice.pub, bob.pub].hash() });;
}

function test_an_invalid_descriptor_can_not_be_added() {
  val auth_descriptor = accounts.auth_descriptor(
    accounts.auth_type.M,
    [
        [accounts.auth_flags.TRANSFER].to_gtv(),
        alice.pub.to_gtv(),
        bob.pub.to_gtv()
    ],
    rules = null.to_gtv()
  );

  assert_fails(
    "Operation 'lib.ft4.external.admin:register_account' failed",
    test.register_account_with_multisig_auth_descriptor(auth_descriptor, [alice, bob], *)
  );
}

function test_duplicate_multisig_ad_can_not_be_used_when_creating_account() {
  val auth_descriptor = accounts.auth_descriptor(
    accounts.auth_type.M,
    [
        [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER].to_gtv(),
        (2).to_gtv(),
        [
          alice.pub,
          alice.pub
        ].to_gtv()
    ],
    rules = null.to_gtv()
  );

  rell.test.tx()
      .op(open.ras_open(auth_descriptor))
      .op(strategies.external.register_account())
      .sign(alice)
      .run_must_fail("duplicate key value violates unique constraint");
}

function test_duplicate_multisig_ad_can_not_be_added_to_account() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .run();

  val auth_descriptor2 = accounts.auth_descriptor(
    accounts.auth_type.M,
    [
        [accounts.auth_flags.TRANSFER].to_gtv(),
        (2).to_gtv(),
        [
          bob.pub,
          bob.pub
        ].to_gtv()
    ],
    rules = null.to_gtv()
  );

  rell.test.tx()
      .op(test.ft_auth_operation_for(alice.pub))
      .op(accounts.external.add_auth_descriptor(auth_descriptor2))
      .sign(alice)
      .sign(bob)
      .run_must_fail("duplicate key value violates unique constraint");
}

function test_multisig_ad_with_zero_required_can_not_be_added_to_account() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .run();

  val auth_descriptor2 = accounts.auth_descriptor(
    accounts.auth_type.M,
    [
        [accounts.auth_flags.TRANSFER].to_gtv(),
        (0).to_gtv(),
        [
          alice.pub,
          bob.pub
        ].to_gtv()
    ],
    rules = null.to_gtv()
  );

  rell.test.tx()
      .op(test.ft_auth_operation_for(alice.pub))
      .op(accounts.external.add_auth_descriptor(auth_descriptor2))
      .sign(alice)
      .sign(bob)
      .run_must_fail("Required signatures must be positive");
}

function test_multisig_ad_with_too_many_required_can_not_be_added_to_account() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  admin.external.register_account(auth_descriptor)
    .sign(test.admin_priv_key())
    .run();

  val auth_descriptor2 = accounts.auth_descriptor(
    accounts.auth_type.M,
    [
        [accounts.auth_flags.TRANSFER].to_gtv(),
        (3).to_gtv(),
        [
          alice.pub,
          bob.pub
        ].to_gtv()
    ],
    rules = null.to_gtv()
  );

  rell.test.tx()
      .op(test.ft_auth_operation_for(alice.pub))
      .op(accounts.external.add_auth_descriptor(auth_descriptor2))
      .sign(alice)
      .sign(bob)
      .run_must_fail("Required signatures cannot be greater than number of signers");
}

function test_create_account_without_auth_in_operation_in_genesis_block() {
  val type = "FEE";  
  val id = (type, chain_context.blockchain_rid).hash();
  
  test_op.register_account_without_auth_test(id, type).run();

  assert_not_null(accounts.account @? { .id == id, .type == type});
}

function test_create_account_without_auth_in_operation_after_genesis_block() {  
  val type = "POOL";  
  val id = (type, chain_context.blockchain_rid).hash();
  
  rell.test.block().run();  
  test_op.register_account_without_auth_test(id, type).run();

  assert_not_null(accounts.account @? { .id == id, .type == type});
}

function test_create_account_without_auth_without_operation_in_genesis_block() {
  val system_account_id = helpers.init_object.system_account_id;
  assert_equals(
    system_account_id, 
    accounts.account @? { 
      .id == chain_context.blockchain_rid, 
      .type == "BLOCKCHAIN"
    }.id
  );
}

function test_create_duplicate_account_without_auth_fails() {
  val type = "FEE";  
  val id = (type, chain_context.blockchain_rid).hash();
  
  test_op.register_account_without_auth_test(id, type).run();
  rell.test.tx()
    .op(test_op.register_account_without_auth_test(id, type))
    .nop()
    .run_must_fail("duplicate key value violates unique constraint");
}

function test_create_account_with_auth_in_operation_in_genesis_block() {
  val auth_descriptor = test.create_auth_descriptor(rell.test.pubkeys.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  test.register_account_with_auth_descriptor(auth_descriptor, rell.test.keypairs.alice);
  assert_equals(accounts.rl_state @ { .account.id == rell.test.pubkeys.alice.hash() } (.last_update), 0);
}

function test_create_account_with_auth_in_operation_after_genesis_block() {
  rell.test.block().run(); // build genesis block to be able to use op_context.last_block_time afterwards

  val auth_descriptor = test.create_auth_descriptor(rell.test.keypairs.alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
  test.register_account_with_auth_descriptor(auth_descriptor, rell.test.keypairs.alice);
  assert_equals(accounts.rl_state @ { .account.id == rell.test.pubkeys.alice.hash() } (.last_update), rell.test.DEFAULT_FIRST_BLOCK_TIME);
}

function test_create_account_auth_in_without_operation_in_genesis_block() {
  val account_id = helpers.init_object.user_account_id;
  assert_equals(accounts.rl_state @ { .account.id == account_id } (.last_update), 0);
}

function test_an_account_cannot_have_too_many_auth_descriptors() {
  val acc_data = test.register_alice();

  val auth_descriptors = [
    test.create_auth_descriptor(alice.pub, ["2"]),
    test.create_auth_descriptor(alice.pub, ["3"]),
    test.create_auth_descriptor(alice.pub, ["4"]),
    test.create_auth_descriptor(alice.pub, ["5"]),
    test.create_auth_descriptor(alice.pub, ["6"]),
    test.create_auth_descriptor(alice.pub, ["7"]),
    test.create_auth_descriptor(alice.pub, ["8"]),
    test.create_auth_descriptor(alice.pub, ["9"]),
    test.create_auth_descriptor(alice.pub, ["10"])
  ];

  val one_too_many = test.create_auth_descriptor(alice.pub, ["Z"]);
  
  var tx = rell.test.tx();
  for (ad in auth_descriptors) {
    tx = tx
      .op(admin.external.add_rate_limit_points(acc_data.account.id, 1))
      .op(test.ft_auth_operation_for(rell.test.pubkeys.alice))
      .op(accounts.external.add_auth_descriptor(ad));
  }
  tx
    .sign(test.admin_priv_key())
    .sign(rell.test.keypairs.alice)
    .run();

  rell.test.tx()
    .op(test.ft_auth_operation_for(rell.test.pubkeys.alice))
    .op(accounts.external.add_auth_descriptor(one_too_many))
    .sign(rell.test.keypairs.alice)
    .run_must_fail("Max <10> auth descriptor count reached. Delete some before adding new ones.");
}

function test_create_account_with_auth_succeeds() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);

  test_op.function__create_account_with_auth(auth_descriptor).run();

  val account_id = accounts.get_account_id_from_signers([alice.pub]);
  assert_not_null(accounts.account @? { .id == account_id });
  assert_not_null(accounts.account_auth_descriptor @? { .account.id == account_id, .id == auth_descriptor.hash() });
}

function test_create_account_with_auth_can_create_account_with_custom_id_when_using_single_sig_auth_descriptor() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);

  val custom_account_id = bob.pub.hash();
  test_op.function__create_account_with_auth(auth_descriptor, custom_account_id).run();

  assert_not_null(accounts.account @? { .id == custom_account_id });
  assert_not_null(accounts.account_auth_descriptor @? { .account.id == custom_account_id, .id == auth_descriptor.hash() });
  assert_null(accounts.account @? { .id == accounts.get_account_id_from_signers([alice.pub])});
}

function test_create_account_with_auth_can_create_account_with_custom_id_when_using_multi_sig_auth_descriptor() {
  val auth_descriptor = test.create_multisig_auth_descriptor(2, [alice.pub, bob.pub], [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);

  val custom_account_id = rell.test.pubkeys.eve.hash();
  test_op.function__create_account_with_auth(auth_descriptor, custom_account_id).run();

  assert_not_null(accounts.account @? { .id == custom_account_id });
  assert_not_null(accounts.account_auth_descriptor @? { .account.id == custom_account_id, .id == auth_descriptor.hash() });
  assert_null(accounts.account @? { .id == accounts.get_account_id_from_signers([alice.pub, bob.pub])});
}

function test_create_account_with_auth_fails_if_mandatory_flag_missing() {
  val auth_descriptor = test.create_auth_descriptor(alice.pub, [accounts.auth_flags.ACCOUNT]);

  test_op.function__create_account_with_auth(auth_descriptor)
    .run_must_fail("Some of mandatory flags <[A, T]> missing, found only <[A]>");
}
