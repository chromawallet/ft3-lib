@test
module;

import lib.ft4.auth;
import test: lib.ft4.test.core;
import test_op: lib.ft4.test.operations;

import auth_test_op: ^^.operations.auth;

/*
 * All tests call `extra_signatures_op` operation that internally calls `auth.verify_signers` function.
 * `verify_signers` verifies signatures for all signers passed to `extra_signatures_op`.
 */

function test_authorizing_operation_with_single_ft_signature_succeeds() {
    val alice = rell.test.keypairs.alice;
    auth_test_op.extra_signatures_op([alice.pub])
        .sign(alice)
        .run();
}

function test_authorizing_operation_with_single_ft_signer_fails_when_signature_is_missing() {
    val alice = rell.test.keypairs.alice;
    auth_test_op.extra_signatures_op([alice.pub])
        .run_must_fail("Missing signature for public key <%s>".format(alice.pub));
}

function test_authorizing_operation_with_ft_signature_fails_when_invalid_signature_provided() {
    val alice = rell.test.keypairs.alice;
    val bob = rell.test.keypairs.bob;
    auth_test_op.extra_signatures_op([alice.pub])
        .sign(bob)
        .run_must_fail("Missing signature for public key <%s>".format(alice.pub));    
}

function test_authorizing_operation_with_many_ft_signatures_succeeds() {
    val alice = rell.test.keypairs.alice;
    val bob = rell.test.keypairs.bob;
    auth_test_op.extra_signatures_op([alice.pub, bob.pub])
        .sign(alice)
        .sign(bob)
        .run(); 
}

function test_authorizing_operation_with_ft_signatures_fails_when_some_signatures_missing() {
    val alice = rell.test.keypairs.alice;
    val bob = rell.test.keypairs.bob;
    auth_test_op.extra_signatures_op([alice.pub, bob.pub])
        .sign(alice)
        .run_must_fail("Missing signature for public key <%s>".format(bob.pub));     
}

function test_authorizing_operation_with_single_evm_signature_succeeds() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = auth_test_op.extra_signatures_op([address]);

    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(op)
        .run();
}

function test_authorizing_operation_with_single_evm_signer_fails_when_signature_is_missing() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    auth_test_op.extra_signatures_op([address])
        .run_must_fail("Missing signature for address <%s>".format(address));
}

function test_authorizing_operation_with_evm_signature_fails_when_invalid_signature_provided() {
    val aliceAddress = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val bob = rell.test.keypairs.bob;

    val op = auth_test_op.extra_signatures_op([aliceAddress]);

    val message = test.create_evm_auth_message(op);
    val bobSignature = test.evm_sign(message, bob.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([aliceAddress], [bobSignature]))
        .op(op)
        .run_must_fail("Invalid signature for address <%s>".format(aliceAddress));
}

function test_authorizing_operation_with_many_evm_signatures_succeeds() {
    val address1 = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val address2 = crypto.eth_privkey_to_address(rell.test.keypairs.bob.priv);

    val op = auth_test_op.extra_signatures_op([address2, address1]);

    val message = test.create_evm_auth_message(op);
    val signature1 = test.evm_sign(message, rell.test.keypairs.alice.priv);
    val signature2 = test.evm_sign(message, rell.test.keypairs.bob.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address1, address2], [signature1, signature2]))
        .op(op)
        .run();    
}

function test_authorizing_operation_fails_when_evm_signatures_provided_in_wrong_order() {
    val address1 = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val address2 = crypto.eth_privkey_to_address(rell.test.keypairs.bob.priv);

    val op = auth_test_op.extra_signatures_op([address2, address1]);

    val message = test.create_evm_auth_message(op);
    val signature1 = test.evm_sign(message, rell.test.keypairs.alice.priv);
    val signature2 = test.evm_sign(message, rell.test.keypairs.bob.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address1, address2], [signature2, signature1]))
        .op(op)
        .run_must_fail("Invalid signature for address <%s>".format(address1));    
}

function test_authorizing_operation_succeeds_with_ft_and_evm_signatures() {
    val aliceAddress = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val bobPubkey = rell.test.keypairs.bob.pub;

    val op = auth_test_op.extra_signatures_op([aliceAddress, bobPubkey]);

    val message = test.create_evm_auth_message(op);
    val aliceSignature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([aliceAddress], [aliceSignature]))
        .op(op)
        .sign(rell.test.keypairs.bob)
        .run();
}

function test_authorizing_operation_with_evm_and_ft_signers_fails_when_evm_signature_is_missing() {
    val aliceAddress = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val bobPubkey = rell.test.keypairs.bob.pub;

    auth_test_op.extra_signatures_op([aliceAddress, bobPubkey])
        .sign(rell.test.keypairs.bob)
        .run_must_fail("Missing signature for address <%s>".format(aliceAddress));
}

function test_authorizing_operation_with_evm_and_ft_signers_fails_when_ft_signature_is_missing() {
    val aliceAddress = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val bobPubkey = rell.test.keypairs.bob.pub;

    val op = auth_test_op.extra_signatures_op([aliceAddress, bobPubkey]);

    val message = test.create_evm_auth_message(op);
    val aliceSignature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([aliceAddress], [aliceSignature]))
        .op(op)
        .run_must_fail("Missing signature for public key <%s>".format(bobPubkey));
}

function test_authorizing_operation_with_evm_signatures_succeeds_when_ft_auth_operation_is_added() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = auth_test_op.extra_signatures_op([address]);

    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(auth.external.ft_auth(rell.test.pubkeys.bob.hash(), rell.test.pubkeys.bob.hash())) // parameter values are not important for the test
        .op(op)
        .run();    
}

function test_authorizing_operation_with_evm_signatures_succeeds_when_evm_auth_operation_is_added() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = auth_test_op.extra_signatures_op([address]);

    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(auth.external.evm_auth(rell.test.pubkeys.bob.hash(), rell.test.pubkeys.bob.hash(), [])) // parameter values are not important for the test
        .op(op)
        .run();    
}

function test_authorizing_operation_with_evm_signatures_fails_when_evm_signature_is_missing() {
    val address1 = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);
    val address2 = crypto.eth_privkey_to_address(rell.test.keypairs.bob.priv);

    val op = auth_test_op.extra_signatures_op([address2, address1]);

    val message = test.create_evm_auth_message(op);
    val signature1 = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address1, address2], [signature1, null]))
        .op(op)
        .run_must_fail("Missing signature for address <%s>".format(address2));        
}

// `verify_signers` expects that `evm_signatures` is preceding operation where `verify_signers` is called,
// or it precedes `ft_auth` or `evm_auth` operation. If other operation is found between `evm_signatures` 
// and operation where `verify_signers` is called, then authorization will fail.
function test_authorizing_operation_with_evm_signatures_fails_when_invalid_operation_is_inserted() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = auth_test_op.extra_signatures_op([address]);

    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(test_op.empty_op())
        .op(op)
        .run_must_fail("Cannot authorize operation <ft4.test.empty_op> with evm_signatures");    
}

function test_authorizing_operation_with_evm_signatures_fails_when_operation_is_not_whitelisted() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = test_op.authenticated_operation();
    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(op)
        .run_must_fail("Cannot authorize operation <%s> with evm_signatures".format(op.name));
}

function test_evm_signatures_throws_error_when_operation_to_authorize_is_missing() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = test_op.authenticated_operation();
    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .run_must_fail("Cannot find an operation to authorize with 'ft4.evm_signatures'");
}

function test_evm_signatures_throws_error_when_operation_to_authorize_is_missing_and_ft_auth_is_provided() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = test_op.authenticated_operation();
    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(auth.external.ft_auth(rell.test.pubkeys.alice.hash(), rell.test.pubkeys.alice.hash()))
        .run_must_fail("Cannot find an operation to authorize with 'ft4.evm_signatures'");
}

function test_evm_signatures_throws_error_when_operation_to_authorize_is_missing_and_evm_auth_is_provided() {
    val address = crypto.eth_privkey_to_address(rell.test.keypairs.alice.priv);

    val op = test_op.authenticated_operation();
    val message = test.create_evm_auth_message(op);
    val signature = test.evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([address], [signature]))
        .op(auth.external.evm_auth(rell.test.pubkeys.alice.hash(), rell.test.pubkeys.alice.hash(), []))
        .run_must_fail("Cannot find an operation to authorize with 'ft4.evm_signatures'");
}