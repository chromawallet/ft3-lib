import { Asset } from "@ft4/asset";
import { Connection, createConnectionToBlockchainRid } from "@ft4/ft-session";
import { BufferId } from "@ft4/utils";
import { Buffer } from "buffer";
import { BlockchainUrlUndefinedException, formatter } from "postchain-client";
import { getAssetOriginById } from "./query-functions";

/**
 * Thrown to indicate that there was an error finding the path to a specific chain for a specific asset.
 */
export class PathfinderError extends Error {
  constructor(msg?) {
    super(msg);
    this.name = "PathfinderError";
  }
}

/**
 * Computes the path, that is a list of intermediate chains, to a specific chain from a source chain.
 * @param connection - connection which will be used to derive the source chain.
 * @param asset - the asset to find the path for
 * @param blockchainRid - the target blockchain rid
 * @param maxPathLength - optional parameter to specify a maximum length of the resulting path. If the computed path is longer than this value, an error will be thrown. Default value is 100.
 * @returns a list of blockchain rids which represents the path from the source to the target for the specified asset.
 */
export async function findPathToChainForAsset(
  connection: Connection,
  asset: Asset,
  blockchainRid: BufferId,
  maxPathLength = 100,
): Promise<Buffer[]> {
  const rootNode = asset.blockchainRid;

  const pathSourceToRoot = [
    formatter.toBuffer(connection.client.config.blockchainRid),
  ];
  const pathEndToRoot = [formatter.ensureBuffer(blockchainRid)];

  let lastNode: Buffer;
  let commonNode: Buffer;
  let isSearchingSource = true;

  let pathLength = 0;
  for (; pathLength < maxPathLength; ++pathLength) {
    const currentArray = isSearchingSource ? pathSourceToRoot : pathEndToRoot;

    lastNode = currentArray[currentArray.length - 1];

    if (!lastNode.equals(rootNode)) {
      // Get the origin chain from the one we're currently exploring.
      // If the config is broken, two scenarios may arise:
      // 1. No origin chain. This call throws.
      // 2. The origin chain is a wrong blockchainRid.
      //    2a. If the chain doesn't exist at all, the next step on this branch will
      //        throw.
      //    2b. If the chain doesn't have the asset, the next step throws.
      //    2c. If the next chain is also the previous one (circular dependency),
      //        this loop would run indefinitely. (might be introduced by malice or error)
      //    2d. If the chain has the asset but no origin blockchainRid, next step is case 1
      //
      // let's consider these things:
      //
      // - set a max search depth for the path to avoid 2c and long trees
      // - check every blockchainRid on the list for circular dependencies to avoid 2c
      //   at the cost of speed
      let tmpConnection: Connection;

      try {
        tmpConnection = await createConnectionToBlockchainRid(
          connection,
          lastNode,
        );
      } catch (error) {
        if (error instanceof BlockchainUrlUndefinedException) {
          throw new PathfinderError(
            `Blockchain ${lastNode.toString(
              "hex",
            )} does not exist on the current network.`,
          );
        } else {
          throw error;
        }
      }

      // three possible errors:
      // 1. query does not exist
      // 2. asset does not exist
      // 3. asset is not a cross-chain asset (origin does not exist)
      //
      // The first two errors are instances of UnexpectedStatusError
      // we either match on the message to rethrow or let it through unhandled
      const nextHop = await getAssetOriginById(tmpConnection, asset.id);

      if (!nextHop) {
        throw new PathfinderError(
          `The asset is not a cross-chain asset on chain ${lastNode.toString(
            "hex",
          )}`,
        );
      }

      currentArray.push(nextHop);
      if (
        (isSearchingSource ? pathEndToRoot : pathSourceToRoot).some((x) =>
          x.equals(nextHop),
        )
      ) {
        commonNode = nextHop;
        break;
      }
    }

    if (pathLength === maxPathLength - 1) {
      throw new PathfinderError(
        `Exceeded max path length of ${maxPathLength} hops. This is most likely due to an error in your code, but if you really need a larger path length, you can increase the default by passing the new max path length as an argument to this function`,
      );
    }

    // switch branch only if the other hasn't reached root node yet
    isSearchingSource = isSearchingSource
      ? pathEndToRoot[pathEndToRoot.length - 1].equals(rootNode)
      : !pathSourceToRoot[pathSourceToRoot.length - 1].equals(rootNode);
  }

  const pathRootToEnd = pathEndToRoot.reverse();

  return pathSourceToRoot
    .slice(
      0,
      pathSourceToRoot.findIndex((x) => x.equals(commonNode)),
    )
    .concat(
      pathRootToEnd.slice(pathRootToEnd.findIndex((x) => x.equals(commonNode))),
    )
    .slice(1); // remove starting chain
}
