import { Buffer } from "buffer";

export type Blockchain = {
  name: string;
  rid: Buffer;
  state: string;
  system: number;
};
