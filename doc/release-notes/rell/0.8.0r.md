## [0.8.0r] - 2024-05-29

### Breaking 💔
- Removed key constraint from `asset` entity `symbol` attribute. 
- Removed `get_asset_by_symbol` (replaced with `get_assets_by_symbol`)
- Updated `register_crosschain_asset` operation and function. Added asset id, asset type and uniqueness resolver arguments.
- Rename `get_paginated_asset_balances_by_name` function to `get_paginated_assets_by_name`
- Removed function `_ft_auth` 
- Removed query `ft4.get_account_by_auth_descriptor`

### Changed 🪙
- Make `asset` entity `icon_url` attribute mutable.

### Added ✅
- Added `uniqueness_resolver` attribute to `asset` entity.
- Added `get_assets_by_symbol` query to get all registered assets with the same symbol.
- Added `get_transfer_rules` query to get transfer strategy rules configuration.


### Fixed 🔧
- Ensure that number of required signatures in multisig auth descriptor is greater than 0.
- Ensure that new main auth descriptors has all mandatory flags when calling `update_main_auth_descriptor`.
