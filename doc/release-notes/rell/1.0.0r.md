## [1.0.0r] - 2024-07-04

### Added ✅
- Added option to filter results in `get_pending_transfer_strategies` based on whether the transfer has expired.

### Fixed 🔧
- Fixed an issue in `get_auth_message_template` query, where query fails if gtv encoded null is provided as `args` value
