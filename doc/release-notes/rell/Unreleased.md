## [Unreleased] - 2024-xx-xx

### Breaking 💔

### Changed 🪙
- Allow multiple smaller transfers to a non-existing account for the same asset type. 

- The logic takes multiple senders for one recipient into consideration

### Added ✅
- Added a blacklist for operations that may not be used after an auth operation (`evm_auth` and `ft_auth`). Defined in the `chromia.yml` as `core.auth.auth_op_blacklisted_operations`, it works the same way as `evm_signatures_authorized_operations`, except the former is a list of operations that are not allowed, while the latter includes only allowed operations.
- Added RellDocs for everything
- Added `get_api_version`. While version looks like "1.0.3", and it's difficult to parse, api version is an integer that is increased by one every time the API (queries and operations) changes. It will start at 1 for version 1.0.1.
- Added `get_block_height`, analogous to `latest_time`. The main difference is that the block height returned is the height of the *next* block to be produced, if in a query. This is also analogous to the difference between `op_context.last_block_time` and `op_context.block_height`, where the height refers to the current block and the time to the last block.

### Fixed 🔧

- `authenticate()` and `authenticate_and_return_context()` will no longer delete the auth descriptor used to authenticate the operation, even if it expired during the authentication process.