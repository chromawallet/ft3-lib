## [1.0.1] - 2024-09-03

### Fixed 🔧

- authentication loop when used with new wagmi version
