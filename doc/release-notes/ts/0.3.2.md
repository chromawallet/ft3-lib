## [0.3.2] - 2024-02-06

### Fixed

- Fixes invalid parameter name for `ft4.get_auth_handler_for_operation` query
